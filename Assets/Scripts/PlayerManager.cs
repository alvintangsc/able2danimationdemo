﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
	Animator animator;

	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator> ();
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		// handling idle to walking state change
		if (Input.GetKeyDown (KeyCode.W)) 
		{
			animator.SetInteger ("state", 1);
		}
		// Key release for walking to idle state change
		if (Input.GetKeyUp (KeyCode.W)) 
		{
			animator.SetInteger ("state", 0);
		}

		// handling idle to walking state change
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			animator.SetInteger ("state", 2);
		}
		// Key release for walking to idle state change
		if (Input.GetKeyUp (KeyCode.R)) 
		{
			animator.SetInteger ("state", 0);
		}
		
	}
}
